import styled from "@emotion/styled";

const Button = styled.button`
    border: solid black 1px;
    padding: .5em 1em;
    outline: none;
    &:hover {
        cursor: pointer;
        background: transparent;
    }
`;

export default Button;