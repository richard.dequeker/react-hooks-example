import { keyframes } from "@emotion/core";
import styled from "@emotion/styled";

const spin = keyframes`
from {
    transform: rotate(0);
}
to {
    transform: rotate(360deg);
}
`;

const Spinner = styled.div`
width: 35px;
height: 35px;
border: solid black 3px;
border-bottom-color: ${props => props.color};
border-radius: 50%;
animation: ${spin} linear 1s infinite;
`;

export default Spinner;