import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import styled from "@emotion/styled";

import { AuthContext } from "../../context/auth";

const Wrapper = styled.div`
background-color: #626262;
display: flex;
justify-content: space-around;
a {
    text-decoration: none;
    color: white;
    &:hover {
        color: gray;
    }
}
`;

const Navigation = () => {
    const { logout } = useContext(AuthContext);
    return (
        <Wrapper>
            <NavLink to="/signin">Signin</NavLink>
            <NavLink to="/register">Register</NavLink>
            <NavLink to="/">Home</NavLink>
            <NavLink to="#" onClick={() => logout()}>Logout</NavLink>
        </Wrapper>
    );
};

export default Navigation; 