import React, { useState } from "react";
import PropTypes from "prop-types";

import Form from "../atoms/Form";
import Input from "../atoms/Input";
import Button from "../atoms/Button";

const SigninForm = ({ onSubmit }) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    return(
        <Form onSubmit={e => {
            e.preventDefault();
            onSubmit(email, password);
        }}>
            <Input value={email} onChange={e => setEmail(e.target.value)} />
            <Input type="password" value={password} onChange={e => setPassword(e.target.value)}/>
            <Button>submit</Button>
        </Form>
    );
};

SigninForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};

export default SigninForm;