const theme = {};

theme.palettes = {
    primary: [
        "#7DE258",
        "#1CB4A4",
        "#40CE0B",
        "#2E9B05",
    ],
    secondary: [
        "#8FD5EF",
        "#ef7d17",
        "#C2A500",
        "#998200"
    ],
    success: [
        "#61CFF9",
        "#5CC4EC",
        "#438FAC",
        "#2A5A6C"
    ],
    danger: [
        "#FF5C5B",
        "#F42D50",
        "#C40000",
        "#9A0000"
    ],
    grayscale: [
        "#ffffff",
        "#f2f2f2",
        "#262626",
        "#1D1D1D"
    ]
};

theme.fonts = {
    primary: "Roboto, sans-serif",
    secondary: "Source Sans Pro, sans-serif",
    pre: "Consolas, Liberation Mono, Menlo, Courier, monospace",
};

export default theme;
