import React from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";

const Wrapper = styled.div`
header {

}
main {

}
footer {
    position: absolute;
    bottom: 0;
    width: 100%;
}
`;

const PageTemplate = ({ header, navigation, children, footer }) => (
    <Wrapper>
        <header>{header}</header>
        <nav>{navigation}</nav>
        <main>{children}</main>
        <footer>{footer}</footer>
    </Wrapper>
);

PageTemplate.propTypes = {
    header: PropTypes.node.isRequired,
    navigation: PropTypes.node,
    children: PropTypes.node,
    footer: PropTypes.node.isRequired
};

export default PageTemplate;