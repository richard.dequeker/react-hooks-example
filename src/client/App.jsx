import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Global, css } from "@emotion/core";
import { hot } from "react-hot-loader";

import { AuthProvider } from "./context/auth";
import MainLayout from "./routes/MainLayout";

const App = () => (
    <>
        <Global styles={css`
        html body {
            margin: 0 auto;
            box-sizing: border-box;
        }
    `} />
        <Router>
            <AuthProvider>
                <MainLayout />
            </AuthProvider>
        </Router>
    </>
);

export default hot(module)(App);