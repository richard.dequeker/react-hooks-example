import * as actionTypes from "./actions";

export default (prevState, { type, payload } ) => {
    switch(type) {
    // signin
    case actionTypes.SIGNIN_REQUEST:
        return { isAuthenticated: false, user: null, token: null };
    case actionTypes.SIGNIN_SUCCESS:
        return { isAuthenticated: true, user: { email: payload.email, roles: payload.roles }, token: payload.token };
    case actionTypes.SIGNIN_ERROR:
        return { isAuthenticated: false, user: null, token: null };
    // logout
    case actionTypes.LOGOUT_REQUEST:
        return { isAuthenticated: false, user: null, token: null };
    // jwt token
    case actionTypes.TOKEN_REQUEST:
        return { isAuthenticated: false, user: null, token: null };
    case actionTypes.TOKEN_SUCCESS:
        return { isAuthenticated: true, user: { email: payload.email, roles: payload.roles }, token: payload.token };
    case actionTypes.TOKEN_ERROR:
        return { isAuthenticated: false, user: null, token: null };
    default:
        return { isAuthenticated: false, user: null, token: null };
    }
};