import React, { createContext, useReducer, useEffect } from "react";
import { decode } from "jsonwebtoken";

import reducer from "./reducer";
import * as actionTypes from "./actions";
export * from "./actions";

const JWT_KEY = "hooks-access-token";

const initialState = {
    isAuthenticated: false,
    user: null,
    token: null,
};

export const AuthContext = createContext(initialState);

export const AuthProvider = props => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const signin = (email, password) => {
        dispatch({ type: actionTypes.SIGNIN_REQUEST });
        fetch("http://localhost:4000/", {
            method: "POST",
            headers: new Headers({ "Content-Type": "application/json" }),
            body: JSON.stringify({ email, password })
        })
            .then(response => response.json())
            .then(({ success, message, authPayload }) => {
                if (!success) {
                    throw new Error(message);
                }
                localStorage.setItem(JWT_KEY, authPayload.token);
                dispatch({ type: actionTypes.SIGNIN_SUCCESS, payload: authPayload });
            })
            .catch(error => {
                dispatch({ type: actionTypes.SIGNIN_ERROR, payload: error.message });
            });
    };

    const logout = () => {
        localStorage.removeItem(JWT_KEY);
        return dispatch({ type: actionTypes.LOGOUT_REQUEST });
    };

    const checkToken = () => {
        if (!state.isAuthenticated) {
            dispatch({ type: actionTypes.TOKEN_REQUEST });
            const token = localStorage.getItem(JWT_KEY);
            if (!token) return dispatch({ type: actionTypes.TOKEN_ERROR });
            const payload = decode(token);
            if (payload) {
                dispatch({ type: actionTypes.TOKEN_SUCCESS, payload: { ...payload, token } });
            } else {
                localStorage.removeItem(JWT_KEY);
            }
        }
    };

    useEffect(() => {
        checkToken();
    });

    return <AuthContext.Provider  {...props} value={{ signin, logout,  ...state }} />;
};