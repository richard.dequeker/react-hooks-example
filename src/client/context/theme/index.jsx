import React, { createContext } from "react";
import PropTypes from "prop-types";

import defaultTheme from "../../components/themes/default";

export const ThemContext = createContext(defaultTheme);

export const ThemeProvider = ({ theme }) => {
    return <ThemContext.Provider value={theme} />;
};

ThemeProvider.propTypes = {
    theme: PropTypes.object.isRequired
};

export const palette = (name, nuance = 1) => props => {
    return props.theme.palettes[name][nuance];
};

export const font = name => props => {
    return props.theme.fonts[name];
};

export const ifProp = (check, ifYes, ifNot) => props => {
    if (typeof check === "string") {
        if(props[check]) {
            return ifYes;
        }
        return ifNot;
    } else {
        if (props[Object.keys(check)[0]] === check[Object.keys(check)[0]]) {
            return ifYes;
        }
        return ifNot;
    }
};
