import React, { useContext } from "react";
import PropTypes from "prop-types";

import PageTemplate from "../components/templates/PageTemplate";
import Header from "../components/organisms/Header";
import Navigation from "../components/organisms/Navigation";
import Footer from "../components/organisms/Footer";
import SigninFrom from "../components/forms/SigninForm";

import {
    AuthContext
} from "../context/auth";

const Signin = () => {
    const { signin } = useContext(AuthContext);
    return (
        <PageTemplate
            header={<Header />}
            navigation={<Navigation />}
            footer={<Footer />}
        >
            <SigninFrom onSubmit={signin} />
        </PageTemplate>
    );
};

Signin.propTypes = {
    history: PropTypes.object.isRequired
};

export default Signin;