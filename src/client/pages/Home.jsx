import React, { useContext } from "react";

import PageTemplate from "../components/templates/PageTemplate";
import Navigation from "../components/organisms/Navigation";
import Header from "../components/organisms/Header";
import Footer from "../components/organisms/Footer";


import { AuthContext } from "../context/auth";

const Home = () => {
    const { isAuthenticated, user } = useContext(AuthContext);
    return (
        <PageTemplate
            header={<Header />}
            navigation={<Navigation links={[{path: "/", name: "Home"}, { path: "/signin", name: "Signin"}]}/>}
            footer={<Footer />}
        >
            <div>Hello user {isAuthenticated ? user.email : ".....oh ! Fuck u ! Ur are  not logged !" }</div>
        </PageTemplate>
    );
};

export default Home;