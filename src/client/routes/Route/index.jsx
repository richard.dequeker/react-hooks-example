import React, { Suspense, useContext } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";

import { Route as ReactRoute } from "react-router-dom";

import { AuthContext } from "../../context/auth";


const Page = ({ fallback, children }) => (
    <Suspense fallback={fallback}>
        {children}
    </Suspense>
);

Page.propTypes = {
    fallback: PropTypes.node,
    children: PropTypes.node
};

Page.defaultProps = {
    fallback: "loading..."
};

const Route = ({ component: Component, role, fallback, ...routeProps}) => {
    const { isAuthenticated, user } = useContext(AuthContext);
    return (
        <ReactRoute {...routeProps} render={props => {
            if (role) {
                if (user && isAuthenticated && user.roles.includes(role)) {
                    return <Page fallback={fallback}><Component {...props} /></Page>;
                } else {
                    return <Redirect to="/signin" />;
                }
            }
            return  <Page fallback={fallback}><Component {...props} /></Page>; 
        }} />
    );
};

export default Route;