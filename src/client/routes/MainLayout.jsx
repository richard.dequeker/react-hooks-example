import React, { lazy } from "react";
import { Switch } from "react-router-dom";

import Route from "./Route";
import Spinner from "../components/atoms/Spinner";

const Home = lazy(() => import("../pages/Home"));
const Signin = lazy(() => import("../pages/Signin"));
const Err404 = lazy(() => import("../pages/Err404"));

const CustomRoute = props => <Route {...props} fallback={<Spinner />} />;

const MainLayout = () => (
    <Switch>
        <CustomRoute role="USER" exact path="/" component={Home}/>
        <CustomRoute path="/signin" component={Signin}/>
        <CustomRoute path="/register" component={Signin}/>
        <CustomRoute component={Err404}/>
    </Switch>
);

export default MainLayout;