import express from "express";
import cors from "cors";
import { sign } from "jsonwebtoken";


const SECRET = "api-secret";
const app = express();

app.use(cors({ origin: "*" }));

app.use(express.json());

app.post("/", (req, res) => {
    const { email, password } = req.body;
    if (!email.length || !password.length) {
        return res.status(422).json({ success: false, message: "Bad input !" });
    }
    const token = sign({ email }, SECRET);
    res.status(200).json({ success: true, authPayload: { email, password, token, roles: ["USER"] } });
});

app.listen(4000);