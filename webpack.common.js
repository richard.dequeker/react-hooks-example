const HTMLPlugin = require("html-webpack-plugin");
const CleanPlugin = require("clean-webpack-plugin");
const nodeExternals = require("webpack-node-externals");

const path = require("path");

const ROOT_DIRECTORY = process.cwd();

module.exports = {
    server: {
        target: "node",
        name: "server",
        externals: [nodeExternals()],
        entry: {
            server: "./src/server"
        },
        output: {
            path: path.resolve(ROOT_DIRECTORY, "dist", "server"),
            filename: "[name].bundle.js"
        },
        plugins: [
            new CleanPlugin(["server"], {
                root: path.resolve(ROOT_DIRECTORY, "dist")
            })
        ],
        resolve: {
            alias: {
                "@models": path.resolve(
                    ROOT_DIRECTORY,
                    "src",
                    "server",
                    "db",
                    "models"
                ),
                "@errors": path.resolve(ROOT_DIRECTORY, "src", "server", "errors"),
                "@middlewares": path.resolve(
                    ROOT_DIRECTORY,
                    "src",
                    "server",
                    "middlewares"
                ),
                "@controllers": path.resolve(
                    ROOT_DIRECTORY,
                    "src",
                    "server",
                    "controllers"
                )
            }
        }
    },
    client: {
        target: "web",
        name: "client",
        entry: {
            client: "./src/client"
        },
        output: {
            path: path.resolve(ROOT_DIRECTORY, "dist", "client"),
            filename: "[name].bundle.js",
            publicPath: "/"
        },
        module: {
            rules: [
                {
                    test: /.(js|jsx)$/,
                    loader: "babel-loader",
                    exclude: /node_modules/
                },
                {
                    test: /\.(sass|s?css)$/,
                    use: ["style-loader", "css-loader", "sass-loader"]
                }
            ]
        },
        plugins: [
            new HTMLPlugin({
                template: "./public/index.html",
                filename: "index.html"
            })
        ],
        resolve: {
            extensions: [".js", ".jsx"],
            alias: {
                "@context": path.resolve(ROOT_DIRECTORY, "src", "client", "context"),
                "@components": path.resolve(
                    ROOT_DIRECTORY,
                    "src",
                    "client",
                    "components"
                )
            }
        }
    }
};
