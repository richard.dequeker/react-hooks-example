const { server, client } = require("./webpack.common");
const NodemonPlugin = require("nodemon-webpack-plugin");
const { HotModuleReplacementPlugin, NamedModulesPlugin } = require("webpack");

// server dev config
server.mode = "development";
server.plugins.push(new NodemonPlugin());
server.watch = true;

client.mode = "development";
client.devtool = "inline-source-map";
client.devServer = {
    hot: true,
    historyApiFallback: true,
    port: 7777,
    open: true,
    publicPath: "/"
};
client.plugins.push(new HotModuleReplacementPlugin(), new NamedModulesPlugin());

module.exports = [server, client];
